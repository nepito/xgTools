FROM islasgeci/base:latest
COPY . /workdir
RUN Rscript -e "install.packages(c('covr', 'devtools', 'lintr', 'roxygen2'), repos='http://cran.rstudio.com')"
RUN R -e "install.packages(c('comprehenr', 'ggpubr', 'jsonify', 'optparse', 'plotly'), repos='http://cran.rstudio.com')"
