library(tidyverse)

metrics <- tibble(
  goal_total = c(10, 20),
  goal_assists = c(15, 8),
  player_minutes = c(5, 2)
)

describe("Normaliza metrics", {
  it("goal for minutes", {
    expected <- tibble(
      goal_total = c(2, 10),
      goal_assists = c(3, 4),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_goal_for_minute()
    expect_equal(obtained, expected)
  })
  it("normalized goals", {
    expected <- tibble(
      goal_total = c(20, 100),
      goal_assists = c(75, 100),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_normalized_goal()
    expect_equal(obtained, expected)
  })
  it("passes for minutes", {
    metrics <- tibble(
      passes_total = c(10, 20),
      passes_key = c(15, 8),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      passes_total = c(2, 10),
      passes_key = c(3, 4),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_passes_for_minute()
    expect_equal(obtained, expected)
  })
  it(" normalized passes", {
    metrics <- tibble(
      passes_total = c(10, 20),
      passes_key = c(15, 8),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      passes_total = c(20, 100),
      passes_key = c(75, 100),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_normalized_passes()
    expect_equal(obtained, expected)
  })
  it("interceptions for minutes", {
    metrics <- tibble(
      tackles_interceptions = c(10, 20),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      tackles_interceptions = c(2, 10),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_interceptions_for_minute()
    expect_equal(obtained, expected)
  })
  it("normalized interceptions", {
    metrics <- tibble(
      tackles_interceptions = c(10, 20),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      tackles_interceptions = c(20, 100),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_normalized_interceptions()
    expect_equal(obtained, expected)
  })
  it("dribbles for minutes", {
    metrics <- tibble(
      dribbles_success = c(10, 20),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      dribbles_success = c(2, 10),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_dribbles_for_minute()
    expect_equal(obtained, expected)
  })
  it("normalized dribbles", {
    metrics <- tibble(
      dribbles_success = c(10, 20),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      dribbles_success = c(20, 100),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_normalized_dribbles()
    expect_equal(obtained, expected)
  })
  it("shots for minutes", {
    metrics <- tibble(
      shots_total = c(10, 20),
      shots_on = c(5, 10),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      shots_total = c(2, 10),
      shots_on = c(1, 5),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_shots_for_minute()
    expect_equal(obtained, expected)
  })
  it("normalized shots", {
    metrics <- tibble(
      shots_total = c(10, 20),
      shots_on = c(5, 10),
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      shots_total = c(20, 100),
      shots_on = c(20, 100),
      player_minutes = c(5, 2)
    )
    obtained <- metrics %>% get_normalized_shots()
    expect_equal(obtained, expected)
  })
  it("normalized minutes", {
    metrics <- tibble(
      player_minutes = c(5, 2)
    )
    expected <- tibble(
      player_minutes = c(100, 40)
    )
    obtained <- metrics %>% get_normalized_minutes()
    expect_equal(obtained, expected)
  })
})

describe("Calculate the discipline", {
  it("Just from card", {
    metrics <- tibble(
      fouls_commited = c(10, 20),
      fouls_drawn = c(10, 20),
      player_lineups = c(30, 20),
      cards = c(5, 2)
    )
    expected <- tibble(
      fouls_commited = c(10, 20),
      fouls_drawn = c(10, 20),
      player_lineups = c(30, 20),
      cards = c(5, 2),
      discipline = c(100, 3000 / 32)
    )
    obtained <- calculate_discipline(metrics)
    expect_equal(obtained, expected)
  })
})


describe("Calculate leadership", {
  it("From minutes", {
    metrics <- tibble(
      team_jj = c(2, 2),
      player_minutes = c(90, 180)
    )
    expected <- tibble(
      team_jj = c(2, 2),
      player_minutes = c(90, 180),
      leadership = c(50, 100)
    )
    obtained <- calculate_leadership_from_minutes(metrics)
    expect_equal(obtained, expected)
  })
  it("From minutes, fouls and goals", {
    metrics <- tibble(
      fouls_commited = c(10, 30),
      fouls_drawn = c(30, 10),
      team_jj = c(2, 2),
      player_minutes = c(90, 180),
      goal_total = c(10, 5),
      goal_assists = c(10, 5),
      team_goals = c(20, 20)
    )
    expected <- tibble(
      fouls_commited = c(10, 30),
      fouls_drawn = c(30, 10),
      team_jj = c(2, 2),
      player_minutes = c(90, 180),
      goal_total = c(10, 5),
      goal_assists = c(10, 5),
      team_goals = c(20, 20),
      leadership = c(100, 15000 / 250)
    )
    obtained <- calculate_leadership(metrics)
    expect_equal(obtained, expected)
  })
})

describe("Get summarized by player", {
  player <- read.csv("../data/Player stats J. Alvarado.csv")
  obtained_summary <- get_summarized_by_player(player)
  it("has the right form", {
    expected_n_row <- 1
    obtained_n_row <- nrow(obtained_summary)
    expect_equal(obtained_n_row, expected_n_row)
    expected_n_col <- 22
    obtained_n_col <- ncol(obtained_summary)
    expect_equal(obtained_n_col, expected_n_col)
  })
  it("Return the right dataframe", {
  })
})
