xgTools

- [N'Golo Kanté: Is he really the best central midfielder?](https://youtu.be/BrV-BMyoOUU)
- [Expected Goals & Points: A detailed explanation + Examples | The importance of xG and xP](https://youtu.be/CCGtjXR4GAk)
- [A Tactical Look At What Went Wrong With Valverde | Can Setien Avoid These Mistakes |
2019/20](https://youtu.be/mToQhV5hN3M)

## Cómo utilizamos este repositorio
- Para calcular los los intervalos de las métricas de los últimos cinco partidos se obtiene de la
  siguiente manera:

```R
Rscript src/data_for_intervals_of_matches_of_a_team.R -i "data/Team Stats Cimarrones de Sonora_last-year.csv" -o metrics_intervals_last-five-matches.csv
```

Aquí necesitaremos los datos del último año, pues límites de las métricas se calculan a partir de
los juegos del último año.