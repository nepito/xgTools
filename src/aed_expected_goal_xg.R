library(tidyverse)
library(data.table)

datos <- read_csv("./data/statisitcs_players_39_2021.csv")

datos %>% summary()
datos$player <- datos$player %>% factor()
datos$position <- datos$position %>% factor()

xG_player <- datos %>%
  group_by(player, position) %>%
  summarize(
    xG = sum(goal_total, na.rm = TRUE),
    partidos = n(),
    minutos = sum(minutes, na.rm = T),
    efectividad = sum(xG / minutos, na.rm = TRUE)
  ) %>%
  arrange(-efectividad) %>%
  filter(minutos >= 900)

xG_player %>% summary()

ggplot(data = xG_player[1:10, ], aes(player, efectividad * 90)) +
  geom_col()

ggplot(data = xG_player[1:10, ], aes(player, 1 / efectividad)) +
  geom_col()

ggplot(data = xG_player) +
  geom_boxplot(mapping = aes(x = position, y = efectividad * 90)) +
  xlab("Posiciones") +
  ylab("Goles por partido")


## Prueba de hipótesis
delantero_medio <- xG_player %>%
  filter(position %in% c("F", "M")) %>%
  ungroup()

t.test(
  delantero_medio %>% filter(position == "F") %>% select(efectividad),
  delantero_medio %>% filter(position == "M") %>% select(efectividad)
)

cuantil <- quantile(delantero_medio %>%
  filter(position == "F") %>%
  .$efectividad, 0.975)

delantero_medio <- delantero_medio %>%
  mutate(min_prom = minutos / partidos, atipico = efectividad > cuantil)

delantero_medio
