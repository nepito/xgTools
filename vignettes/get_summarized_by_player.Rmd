---
title: "my-vignette"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{my-vignette}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

Cargamos la librería

```{r setup}
library(xgTools)
```

Cargamos la tabla de datos del jugadador y usamos la función para obtener la suma de sus columnas numéricas y crear la tabla que va a ir al diagrama chingón de estrella.

```{r data}
datos <- read.csv("tests/data/Player stats J. Alvarado.csv")

summarized_data <- get_summarized_by_player(datos)
```
